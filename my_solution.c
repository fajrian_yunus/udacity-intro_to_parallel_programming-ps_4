//Udacity HW 4
//Radix Sorting

#include "reference_calc.cpp"
#include "utils.h"
#include "stdio.h"

/* Red Eye Removal
   ===============
   
   For this assignment we are implementing red eye removal.  This is
   accomplished by first creating a score for every pixel that tells us how
   likely it is to be a red eye pixel.  We have already done this for you - you
   are receiving the scores and need to sort them in ascending order so that we
   know which pixels to alter to remove the red eye.

   Note: ascending order == smallest to largest

   Each score is associated with a position, when you sort the scores, you must
   also move the positions accordingly.

   Implementing Parallel Radix Sort with CUDA
   ==========================================

   The basic idea is to construct a histogram on each pass of how many of each
   "digit" there are.   Then we scan this histogram so that we know where to put
   the output of each digit.  For example, the first 1 must come after all the
   0s so we have to know how many 0s there are to be able to start moving 1s
   into the correct position.

   1) Histogram of the number of occurrences of each digit
   2) Exclusive Prefix Sum of Histogram
   3) Determine relative offset of each digit
        For example [0 0 1 1 0 0 1]
                ->  [0 1 0 1 2 3 2]
   4) Combine the results of steps 2 & 3 to determine the final
      output location for each element and move it there

   LSB Radix sort is an out-of-place sort and you will need to ping-pong values
   between the input and output buffers we have provided.  Make sure the final
   sorted results end up in the output buffer!  Hint: You may need to do a copy
   at the end.

 */

__global__ void add_number(unsigned int* d_array, const unsigned int the_number, const size_t numElems) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId < numElems) {
        d_array[myId] += the_number;
    }    
}

__global__ void d_invert(unsigned int* d_source, unsigned int* d_target, const size_t numElems) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId < numElems) {
        d_target[myId] = d_source[myId] ^ 1;
    }    
}

__global__ void get_binary_digits(unsigned int* const d_inputVals,
                                  unsigned int* d_binaryDigits,
                                  const unsigned int numberOfRightShifts,
                                  const size_t numElems) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId < numElems) {
        d_binaryDigits[myId] = (d_inputVals[myId] >> numberOfRightShifts) & 1;
    }
}

__global__ void exclusive_belloch_scan_reduce_step(unsigned int* d_binaryDigits_exclusiveSum_power_of_two,
                                         const size_t numElems_power_of_two,
                                         const unsigned int multiplier) {
    unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId >= numElems_power_of_two) {
        return;
    }
    
    unsigned int idxOneBase = myId+1;
    if (idxOneBase % multiplier != 0) {
        return;
    }
    unsigned int right = d_binaryDigits_exclusiveSum_power_of_two[myId];
    unsigned int left = d_binaryDigits_exclusiveSum_power_of_two[idxOneBase - multiplier / 2 - 1];
    d_binaryDigits_exclusiveSum_power_of_two[myId] = left + right;
}

__global__ void exclusive_belloch_scan_zeroing_step(
                                            unsigned int* d_binaryDigits_exclusiveSum_power_of_two,
                                             const size_t numElems_power_of_two) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId == numElems_power_of_two-1) {
        d_binaryDigits_exclusiveSum_power_of_two[myId] = 0;
    }
}

__global__ void exclusive_belloch_scan_downsweep_step(unsigned int* d_binaryDigits_exclusiveSum_power_of_two,
                                           const size_t numElems_power_of_two,
                                           const unsigned int multiplier) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    unsigned int idxOneBase = myId+1;
    if (myId >= numElems_power_of_two) {
        return;
    }
    if (idxOneBase % multiplier != 0) {
        return;
    }
    unsigned int right = d_binaryDigits_exclusiveSum_power_of_two[myId];
    unsigned int left = d_binaryDigits_exclusiveSum_power_of_two[idxOneBase - multiplier / 2 - 1];    
    d_binaryDigits_exclusiveSum_power_of_two[idxOneBase - multiplier / 2 - 1] = right;
    d_binaryDigits_exclusiveSum_power_of_two[myId] = left + right;
}

unsigned int get_the_nearest_power_of_two(const unsigned int in) {
    unsigned int out = 1;
    while (out < in) {
        out *= 2;
    }
    return out;
}

__global__ void kernel_create_d_binaryDigits_power_of_two(unsigned int* d_binaryDigits, 
                                        unsigned int* d_binaryDigits_power_of_two,  
                                        const size_t numElems, 
                                        const unsigned int nearest_power_of_two) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId >= nearest_power_of_two) {
        return;
    } else if (myId >= numElems) {
        d_binaryDigits_power_of_two[myId] = 0;
    } else {
        d_binaryDigits_power_of_two[myId] = d_binaryDigits[myId];
    }
}

__global__ void vector_addition(unsigned int* v1, unsigned int* v2, 
                                unsigned int* output, const unsigned int length) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId < length) {
        output[myId] = v1[myId] + v2[myId];
    }
}

void inclusive_belloch_scan(const unsigned int nearest_power_of_two,
                           const unsigned int numberOfBlocks,
                           const unsigned int threadsPerBlock,
                           unsigned int* d_binaryDigits_power_of_two,
                           unsigned int* d_binaryDigits_inclusiveSum_power_of_two,
                           const unsigned int extra_addition) {
        //belloch scan reduce
        for (unsigned int multiplier = 2 ; multiplier <= nearest_power_of_two ; multiplier *= 2) {            
            exclusive_belloch_scan_reduce_step<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(
                d_binaryDigits_inclusiveSum_power_of_two,
                nearest_power_of_two,
                multiplier
            );          
        }
    
        //belloch scan zeroing
        exclusive_belloch_scan_zeroing_step<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(
                                            d_binaryDigits_inclusiveSum_power_of_two,
                                            nearest_power_of_two);
        
        //belloch scan reduce
        for (unsigned int multiplier = nearest_power_of_two ; multiplier > 1 ; multiplier /= 2) {
            exclusive_belloch_scan_downsweep_step<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(
                                               d_binaryDigits_inclusiveSum_power_of_two,
                                               nearest_power_of_two,
                                               multiplier);        
        }    
    
        add_number<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(
                                            d_binaryDigits_inclusiveSum_power_of_two,
                                            extra_addition,
                                            nearest_power_of_two);   
    
        unsigned int* temp;
        checkCudaErrors(cudaMalloc(&temp, sizeof(unsigned int) * nearest_power_of_two));
        vector_addition<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(
                                d_binaryDigits_power_of_two,
                                d_binaryDigits_inclusiveSum_power_of_two, 
                                temp, nearest_power_of_two);
        checkCudaErrors(cudaMemcpy((void*)d_binaryDigits_inclusiveSum_power_of_two, (void*) temp, sizeof(unsigned int) * nearest_power_of_two, cudaMemcpyDeviceToDevice));
        checkCudaErrors(cudaFree(temp));
}

__global__ void d_get_last_element(unsigned int* d_array, const unsigned int length, unsigned int* d_answer) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    
    if (length == 0) {
        return;
    }
    
    if (myId == length - 1) {
        d_answer[0] = d_array[myId];
    }
}

unsigned int get_last_element(unsigned int* d_array, const unsigned int length,
                              const unsigned int numberOfBlocks,
                               const unsigned int threadsPerBlock) {

    unsigned int* d_output;
    checkCudaErrors(cudaMalloc(&d_output, sizeof(unsigned int)));
    d_get_last_element<<<dim3(threadsPerBlock), dim3(numberOfBlocks)>>>(
        d_array,
        length,
        d_output
    );
    unsigned int* h_output = (unsigned int*) malloc(sizeof(unsigned int));
    checkCudaErrors(cudaMemcpy((void *)  h_output, (void *)  d_output, sizeof(unsigned int), cudaMemcpyDeviceToHost));
    unsigned int output = h_output[0];
    free(h_output);
    checkCudaErrors(cudaFree(d_output));
    return output;
}

__global__ void map(unsigned int* in, unsigned int* out,
                    unsigned int* zero_indices, unsigned int* one_indices,
                    const unsigned int ones_offset, const unsigned int length,
                    unsigned int* const d_currentPost, unsigned int* const d_newtPost) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    
    if (myId == 0) {
        //zero
        unsigned int my_value_zero = zero_indices[myId];
        if (my_value_zero > 0) {
            out[my_value_zero-1] = in[myId];
            d_newtPost[my_value_zero-1] = d_currentPost[myId];
        }
        
        //one
        unsigned int my_value_one = one_indices[myId];
        if (my_value_one > ones_offset) {
            out[my_value_one-1] = in[myId];
            d_newtPost[my_value_one-1] = d_currentPost[myId];
        }
    } else {
        //zero
        unsigned int left_neighbour_zeros = zero_indices[myId - 1];
        unsigned int my_value_zero = zero_indices[myId];
        if (my_value_zero > left_neighbour_zeros) {
            out[my_value_zero-1] = in[myId];
            d_newtPost[my_value_zero-1] = d_currentPost[myId];
        }
        
        //one
        unsigned int left_neightbour_ones = one_indices[myId - 1];
        unsigned int my_value_one = one_indices[myId];
        if (my_value_one > left_neightbour_ones) {
            out[my_value_one-1] = in[myId];
            d_newtPost[my_value_one-1] = d_currentPost[myId];
        }
    }
}

__global__ void fillSerial(unsigned int* d_array, const unsigned int length) {
    const unsigned int myId = threadIdx.x + blockDim.x * blockIdx.x;
    if (myId < length) {
        d_array[myId] = myId;
    }
}

void your_sort(unsigned int* const d_inputVals,
               unsigned int* const d_inputPos,
               unsigned int* const d_outputVals,
               unsigned int* const d_outputPos,
               const size_t numElems)
{
    //the numbers are 32 bits
    printf("numElems = %d\n", numElems);
    
    const unsigned int nearest_power_of_two = get_the_nearest_power_of_two(numElems);
    printf("nearest_power_of_two = %d\n", nearest_power_of_two);
    
    const unsigned int threadsPerBlock = 512;
    unsigned int numberOfBlocks = nearest_power_of_two / threadsPerBlock;
    if (threadsPerBlock * numberOfBlocks < numElems) {
        numberOfBlocks++;
    }
    
    unsigned int* h_inputVals = (unsigned int*) malloc(numElems * sizeof(unsigned int));
    checkCudaErrors(cudaMemcpy((void *)  h_inputVals, (void *)  d_inputVals, sizeof(unsigned int) * numElems, cudaMemcpyDeviceToHost));
    
    checkCudaErrors(cudaMemcpy((void*) d_outputVals, (void*) d_inputVals, sizeof(unsigned int) * numElems, cudaMemcpyDeviceToDevice));
    checkCudaErrors(cudaMemcpy((void*) d_outputPos, (void*) d_inputPos, sizeof(unsigned int) * numElems, cudaMemcpyDeviceToDevice));
 
    for (unsigned int numberOfRightShifts = 0 ; numberOfRightShifts < 32 ; numberOfRightShifts++) {        
        unsigned int* d_binaryDigits;
        checkCudaErrors(cudaMalloc(&d_binaryDigits, sizeof(unsigned int) * numElems));
        get_binary_digits<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(d_outputVals, d_binaryDigits, numberOfRightShifts, numElems);
        
        unsigned int* d_binaryDigits_power_of_two;
        checkCudaErrors(cudaMalloc(&d_binaryDigits_power_of_two, sizeof(unsigned int) * nearest_power_of_two)); 
        kernel_create_d_binaryDigits_power_of_two<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>
                                        (d_binaryDigits, 
                                        d_binaryDigits_power_of_two,  
                                        numElems, 
                                        nearest_power_of_two);
        
        unsigned int* d_binaryDigits_inverted;
        checkCudaErrors(cudaMalloc(&d_binaryDigits_inverted, sizeof(unsigned int) * numElems));
        d_invert<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(
            d_binaryDigits, d_binaryDigits_inverted, numElems);
        
        unsigned int* d_binaryDigits_inverted_power_of_two;
        checkCudaErrors(cudaMalloc(&d_binaryDigits_inverted_power_of_two, sizeof(unsigned int) * nearest_power_of_two));
        
        kernel_create_d_binaryDigits_power_of_two<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>
                                        (d_binaryDigits_inverted, 
                                        d_binaryDigits_inverted_power_of_two,  
                                        numElems, 
                                        nearest_power_of_two);        
        
        unsigned int* d_binaryDigits_zero_inclusiveSum_power_of_two;
        checkCudaErrors(cudaMalloc(&d_binaryDigits_zero_inclusiveSum_power_of_two, sizeof(unsigned int) * nearest_power_of_two));
        checkCudaErrors(cudaMemcpy((void*) d_binaryDigits_zero_inclusiveSum_power_of_two, (void*) d_binaryDigits_inverted_power_of_two, sizeof(unsigned int) * nearest_power_of_two, cudaMemcpyDeviceToDevice));
        inclusive_belloch_scan(nearest_power_of_two,
                           numberOfBlocks,
                           threadsPerBlock,
                           d_binaryDigits_inverted_power_of_two,
                           d_binaryDigits_zero_inclusiveSum_power_of_two,
                           0);      
                
        unsigned int ones_offset = get_last_element(
            d_binaryDigits_zero_inclusiveSum_power_of_two, 
            nearest_power_of_two,
            numberOfBlocks,
            threadsPerBlock);

        unsigned int* d_binaryDigits_one_inclusiveSum_power_of_two;
        checkCudaErrors(cudaMalloc(&d_binaryDigits_one_inclusiveSum_power_of_two, sizeof(unsigned int) * nearest_power_of_two));
        checkCudaErrors(cudaMemcpy((void*) d_binaryDigits_one_inclusiveSum_power_of_two, (void*) d_binaryDigits_power_of_two, sizeof(unsigned int) * nearest_power_of_two, cudaMemcpyDeviceToDevice));
        inclusive_belloch_scan(nearest_power_of_two,
                           numberOfBlocks,
                           threadsPerBlock,
                           d_binaryDigits_power_of_two,
                           d_binaryDigits_one_inclusiveSum_power_of_two,
                           ones_offset);
        
        unsigned int* one_round_sorted;
        checkCudaErrors(cudaMalloc(&one_round_sorted, sizeof(unsigned int) * numElems));
        
        unsigned int* d_new_post_temp;
        checkCudaErrors(cudaMalloc(&d_new_post_temp, sizeof(unsigned int) * numElems));
        
        map<<<dim3(numberOfBlocks), dim3(threadsPerBlock)>>>(
            d_outputVals,
            one_round_sorted,
            d_binaryDigits_zero_inclusiveSum_power_of_two,
            d_binaryDigits_one_inclusiveSum_power_of_two,
            ones_offset,
            numElems,
            d_outputPos,
            d_new_post_temp
        );
        
        checkCudaErrors(cudaMemcpy((void*) d_outputVals, (void*) one_round_sorted, sizeof(unsigned int) * numElems, cudaMemcpyDeviceToDevice));
        checkCudaErrors(cudaMemcpy((void*) d_outputPos, (void*) d_new_post_temp, sizeof(unsigned int) * numElems, cudaMemcpyDeviceToDevice));
                
        checkCudaErrors(cudaFree(one_round_sorted));
        checkCudaErrors(cudaFree(d_new_post_temp));
        checkCudaErrors(cudaFree(d_binaryDigits_one_inclusiveSum_power_of_two));
        checkCudaErrors(cudaFree(d_binaryDigits_zero_inclusiveSum_power_of_two));
        checkCudaErrors(cudaFree(d_binaryDigits_inverted_power_of_two));
        checkCudaErrors(cudaFree(d_binaryDigits_inverted));
        checkCudaErrors(cudaFree(d_binaryDigits_power_of_two));
        checkCudaErrors(cudaFree(d_binaryDigits));

    }
    
    free(h_inputVals);
}

